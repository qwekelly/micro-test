## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm serve
```

### Lints and auto fixes files
```
npm run lint
npm run lint2
```

### Tests and update snapshot and clear cache
```
npm run test
npm run test-update
npm run test-clearCache
```

### Compiles and minifies for production
```
npm run build
```

### Directory Layout
```
.
├── /0-materials/                # UI resources, put SVG files here
├── /public/                     # Static files which are copied into the /dist folder
├── /src/                        # The source code of the application
│   ├── /components/             # Top level vue components
│   ├── /layouts/                # Layouts for different views
│   │   ├── /outline/            # Layout outline
│   │   └── /default/            # Default Layout
|   |   /libs/                   # Third party Library
│   ├── /routes/                 # Routes and pages
│   │   └── /xxx/                # Xxx page
│   ├── /service/                # Api service interface entry
│   ├── /store/                  # Vuex store
│   ├── /styles/                 # Global stylesheets
│   ├── /utils/                  # Utility library
│   ├── /App.vue                 # Vue app entry
│   ├── /main.js                 # Client entry
│   └── /router.js               # Vue router entry
├── /test/unit/                  # test files
├── /tools/                      # Build automation scripts and utilities
└── package.js                   # The list of 3rd party libraries and utilities
```

#### Other Tips

* 项目框架是由vue-cli构建的，请先安装vue2版本的vue-cli。
* 项目支持webp图片显示，styles封装兼容iphonex设备公用mixin函数。
* 个人管理页面可以切换用户角色，模拟对不同用户实现灰度发布，角色身份存储在store。
* 项目引入jest，结合vue-jest实现单元测试来提高代码质量。
* 项目引入pre-commit，在push代码时对eslint、jest进行检测，不通过将commit不成功。
* 项目涉及需配置化数据均由service/user接口文件mock返回。
* gitlab-ci.yml 结合ci/cd实现自动化。(这里我没有部署服务器的操作，自动化脚本只执行eslint检测、test单元测试、和build构建)

