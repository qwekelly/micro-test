const webpack = require('webpack')
const path = require('path')

module.exports = {
  publicPath: '/',
  assetsDir: 'static',
  crossorigin: 'anonymous',

  configureWebpack: {
    entry: ['babel-polyfill', './src/main.js'],
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        'vue$': 'vue/dist/vue.esm.js',
        'styles': path.join(__dirname, 'src/styles'),
        'images': path.join(__dirname, 'src/images'),
        'routes': path.join(__dirname, 'src/routes'),
        '@': path.join(__dirname, 'src')
      },
      unsafeCache: true,
      symlinks: false
    },
    plugins: [
      new webpack.DefinePlugin({
        __DEV__: !process.env.ENV || process.env.ENV === 'dev',
        __TEST__: process.env.ENV === 'test',
        __PRE__: process.env.ENV === 'pre',
        __ONLINE__: process.env.ENV === 'online',
        __BUILD__: process.env.NODE_ENV === 'production'
      })
    ]
  },

  productionSourceMap: process.env.ENV !== 'online',

  chainWebpack: config => {
    config.plugins.delete('prefetch')
    config.plugins.delete('preload')
    config.optimization.minimizer('terser').tap(args => {
      const options = args[0]
      options.terserOptions.compress.drop_console = false
      options.terserOptions.compress.drop_debugger = false
      return args
    })
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap(options => Object.assign(options, { limit: 0 }))
  },

  css: {
    sourceMap: process.env.ENV !== 'online' // 关闭可以提升构建速度
  }
}
