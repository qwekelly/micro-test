module.exports = api => {
  const isTest = api.env('test')
  // You can use isTest to determine what presets and plugins to use.
  // Jest will set process.env.NODE_ENV to 'test'
  if (isTest) {
    return {
      env: {
        test: {
          presets: [
            [
              '@babel/preset-env',
              {
                targets: {
                  node: 'current'
                }
              }
            ]
          ]
        }
      }
    }
  } else {
    return {
      presets: ['@vue/app'],
      plugins: [
        '@babel/plugin-syntax-dynamic-import'
      ]
    }
  }
}
