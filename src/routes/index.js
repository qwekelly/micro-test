import outlineLayout from '../layouts/outline'
import errors from './errors'
import site from './site'
import user from './user'

let fallbackRouter = {
  path: '*',
  component: outlineLayout,
  children: [errors]
}

let baseRouter = {
  path: '/',
  component: outlineLayout,
  children: [
    {
      path: '',
      meta: {
        title: '首页'
      },
      component: () => import('./site/home')
    },
    site,
    user
  ]
}

let routes = [baseRouter]

// 这个要放在最后
routes.push(fallbackRouter)

export default routes
