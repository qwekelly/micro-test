import defaultLayout from '../../layouts/default'

export default {
  path: 'site',
  component: defaultLayout,
  children: [
    {
      path: 'home',
      meta: {
        title: '首页'
      },
      component: () => import('./home')
    }
  ]
}
