import { getHolidayNotice, getEntryConfig, getCommonProblemList } from '@/services/user.js'
import { mapState } from 'vuex'

export default {
  data() {
    return {
      noticeDetail: {
        noticeText: ''
      },
      entryConfigs: [],
      problemsList: []
    }
  },
  computed: {
    ...mapState(['user'])
  },
  created() {
    this.getNoticesDetail()
    this.getEntryInfo()
    this.getProblemsList()
  },
  methods: {
    getNoticesDetail() {
      return getHolidayNotice().then(res => {
        if (res.data && res.data.notice_text) {
          this.noticeDetail.noticeText = res.data.notice_text
        }
      })
    },
    getEntryInfo() {
      return getEntryConfig(this.user.userType).then(res => {
        if (res.data && res.data.length) {
          this.entryConfigs = res.data
        }
      })
    },
    getProblemsList() {
      return getCommonProblemList().then(res => {
        if (res.data && res.data.length) {
          this.problemsList = res.data
        }
      })
    }
  }
}
