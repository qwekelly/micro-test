import defaultLayout from '../../layouts/default'

export default {
  path: 'user',
  component: defaultLayout,
  children: [
    {
      path: 'main',
      component: () => import('./main')
    }
  ]
}
