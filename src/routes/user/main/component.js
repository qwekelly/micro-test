import { mapState, mapMutations } from 'vuex'

export default {
  data() {
    return {
      radioVal: ''
    }
  },
  computed: {
    ...mapState(['user'])
  },
  created() {
    this.radioVal = this.user.userType
  },
  methods: {
    ...mapMutations({
      updateUserType: 'user/setUserType'
    }),
    onSelectUserType() {
      this.updateUserType(this.radioVal)
    }
  }
}
