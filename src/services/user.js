// 接口 mock 数据

/**
 * 获取节假日公告信息
 * @returns object
 */

export const getHolidayNotice = () => {
  return Promise.resolve({
    status: 1,
    data: {
      notice_text: '潮州银行劳动节假期节日安排'
    }
  })
}

/**
 * 根据用户类型获取不同的入口配置信息（灰度发布）
 * @param {string} userType
 * @returns object
 */

export const getEntryConfig = (userType) => {
  let reBackData = []
  if (userType === 'a') {
    reBackData = [{
      type: 'global_collection',
      title: '全球收款',
      desc: '便捷收款更快速'
    }]
  } else {
    reBackData = [{
      type: 'global_collection',
      title: '全球收款',
      desc: '便捷收款更快速'
    }, {
      type: 'study_pay',
      title: '留学缴费',
      desc: '免购汇更省心'
    }]
  }
  return Promise.resolve({
    status: 1,
    data: reBackData
  })
}

/**
 * 获取常见问题列表
 * @returns object
 */

export const getCommonProblemList = () => {
  return Promise.resolve({
    status: 1,
    data: [{
      problem_title: '如何使用微汇款进行留学生缴费',
      problem_id: '1'
    }, {
      problem_title: '接收汇款的流程是什么？',
      problem_id: '2'
    }, {
      problem_title: '留学缴费需要准备 什么材料？',
      problem_id: '3'
    }]
  })
}

/**
 * 查询订单待收款数量
 * @returns object
 */

export const getOrderCollections = () => {
  return Promise.resolve({
    status: 1,
    data: {
      collection_num: 2
    }
  })
}
