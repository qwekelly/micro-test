import helloWorld from './helloworld'

export default {
  install: function (Vue) {
    Vue.component(helloWorld.name, helloWorld)
  }
}
