
const state = () => {
  return {
    userType: 'b'
  }
}

const mutations = {
  setUserType(state, data) {
    state.userType = data
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
