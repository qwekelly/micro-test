import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import getters from './getters'
import modules from './modules'

Vue.use(Vuex)

let store = new Vuex.Store({
  actions,
  getters,
  modules,
  plugins: []
})

if (!__BUILD__) {
  if (module.hot) {
    module.hot.accept([
      './actions',
      './getters',
      './modules'
    ], () => {
      store.hotUpdate({
        actions: require('./actions').default,
        getters: require('./getters').default,
        modules: require('./modules').default
      })
    })
  }
}

export default store
