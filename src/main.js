import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// global components
import components from './components'

import './styles/global.scss'
import './libs/modernizr-custom'

Vue.use(components)

window.Modernizr.on('webp', (result) => {
  console.log('webp支持度', result)
})

Vue.config.productionTip = false
Vue.config.errorHandler = function (err) {
  throw new Error(err)
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
