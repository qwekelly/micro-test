/* eslint-disable no-undef */
import { shallowMount } from '@vue/test-utils'
import options from '@test/unit/before_test'
import Component from '@/routes/site/home/index.vue'
import { getHolidayNotice, getEntryConfig, getCommonProblemList } from '@/services/user'

jest.mock('@/services/user')

options.store = {
  state: {
    user: {
      userType: 'b'
    }
  }
}

describe('首页', () => {
  let wrapper

  // 存储初始化依赖函数
  let originGetNoticesDetail = Component.methods.getNoticesDetail
  let originGetEntryInfo = Component.methods.getEntryInfo
  let originGetProblemsList = Component.methods.getProblemsList

  let getNoticesDetail
  let getEntryInfo
  let getProblemsList

  beforeEach(() => {
    // mock 初始化依赖函数
    getNoticesDetail = jest.fn()
    Component.methods.getNoticesDetail = getNoticesDetail
    getEntryInfo = jest.fn()
    Component.methods.getEntryInfo = getEntryInfo
    getProblemsList = jest.fn()
    Component.methods.getProblemsList = getProblemsList

    wrapper = shallowMount(Component, Object.assign({}, options, {
      mocks: {}
    }))
  })

  test('初始化以及快照', () => {
    // 初始 mock 函数调用次数
    expect(getNoticesDetail.mock.calls.length).toBe(1)
    expect(getEntryInfo.mock.calls.length).toBe(1)
    expect(getProblemsList.mock.calls.length).toBe(1)
    // 快照
    expect(wrapper.element).toMatchSnapshot()
  })

  test('获取节假日公告通知信息', () => {
    let noticeInfo = {
      data: {
        notice_text: '潮州银行劳动节假期节日安排'
      }
    }
    getHolidayNotice.mockResolvedValueOnce(noticeInfo)
    wrapper.vm.getNoticesDetail = originGetNoticesDetail
    return wrapper.vm.getNoticesDetail().then(() => {
      expect(wrapper.vm.noticeDetail.noticeText).toBe(noticeInfo.data.notice_text)
    })
  })

  test('根据用户类型获取入口配置信息：a 用户', () => {
    wrapper.vm.$store.state.user.userType = 'a'
    let entryInfo = {
      data: [{
        type: 'global_collection',
        title: '全球收款',
        desc: '便捷收款更快速'
      }]
    }
    getEntryConfig.mockResolvedValueOnce(entryInfo)
    wrapper.vm.getEntryInfo = originGetEntryInfo
    return wrapper.vm.getEntryInfo().then(() => {
      expect(wrapper.vm.entryConfigs.length).toBe(entryInfo.data.length)
      expect(wrapper.vm.entryConfigs).toEqual(entryInfo.data)
    })
  })

  test('根据用户类型获取入口配置信息：b 用户', () => {
    wrapper.vm.$store.state.user.userType = 'b'
    let entryInfo = {
      data: [{
        type: 'global_collection',
        title: '全球收款',
        desc: '便捷收款更快速'
      }, {
        type: 'study_pay',
        title: '留学缴费',
        desc: '免购汇更省心'
      }]
    }
    getEntryConfig.mockResolvedValueOnce(entryInfo)
    wrapper.vm.getEntryInfo = originGetEntryInfo
    return wrapper.vm.getEntryInfo().then(() => {
      expect(wrapper.vm.entryConfigs.length).toBe(entryInfo.data.length)
      expect(wrapper.vm.entryConfigs).toEqual(entryInfo.data)
    })
  })

  test('获取常见问题信息', () => {
    let problemInfo = {
      data: [{
        problem_title: '如何使用微汇款进行留学生缴费',
        problem_id: '1'
      }]
    }
    getCommonProblemList.mockResolvedValueOnce(problemInfo)
    wrapper.vm.getProblemsList = originGetProblemsList
    return wrapper.vm.getProblemsList().then(() => {
      expect(wrapper.vm.problemsList.length).toBe(problemInfo.data.length)
      expect(wrapper.vm.problemsList).toEqual(problemInfo.data)
    })
  })
})
