import { createLocalVue } from '@vue/test-utils'
import components from '@/components'
import store from '@/store'

const Vue = createLocalVue()

Vue.use(components)
Vue.config.productionTip = false

window.Modernizr = {
  webp: false
}

export default {
  store,
  localVue: Vue
}
