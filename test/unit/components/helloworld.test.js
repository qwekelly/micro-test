/* eslint-disable no-undef */
import { mount } from '@vue/test-utils'
import options from '@test/unit/before_test'
import Component from '@/components/helloworld/index.vue'

describe('helloworld组件', () => {
  test('match snapshot', () => {
    const wrapper = mount(Component, options)
    expect(wrapper.element).toMatchSnapshot()
  })
})
